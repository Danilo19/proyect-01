package com.example.daniloa.proyect_01

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.os.CountDownTimer
import android.widget.Toast

class MainProyect01 : AppCompatActivity() {

    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeleftTextView: TextView
    internal lateinit var tapMeButton: Button
    internal var score = 0
    internal var gameStarted = false
    internal lateinit var countDownTimer: countDownTimer
    internal val initialCountDown = 60000L
    internal val timeLeft = 60

    internal val TAG = GameMainActivity::class.java.simpleName

    companion object {
        private val SCORE_KEY = "SCORE_KEY"
        private val TIME_LEFT_KEY = "TIME_LEFT_KEY"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_proyect01)


        gameScoreTextView = findViewById<TextView>(R.id.game_score_text_view)
        timeleftTextView = findViewById<TextView>(R.id.time_left_text_view)
        tapMeButton = findViewById<Button>(R.id.tap_me_button)

        resetGame()

        tapMeButton.setOnClickListener { _ -> incrementScore() }

    }

    private fun resetGame() {

        score = 0
        timeLeft = 60

        val gameScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = gameScore

        val timeLeftText = getString(R.string.time_left, Integer.toString(timeLeft))
        timeleftTextView.text = timeLeftText

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInternal)
                override fun onTick(millisUntilFinished: Long){
            timeLeft = millisUntilFinished.toInt() / 1000
            timeleftTextView.text = getString(R.string.time_left, Integer.toString(timeLeft))
        }

        private fun startGame() {
            countDownTimer.start()
            gameStarted = true
        }
    }

    private fun incrementScore() {
        score ++
        //val newScore = "Your Score: " + Integer.toString(score)
        val newScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = newScore

        if (!gameStarted){
            startGame()
        }
    }

    private fun startgame() {
        countDownTimer.start()
        gameStarted = true
    }

    private fun endGame() {

        Toast.makeText(context: this, text: "Your final score is $score", Toast.LENGTH_LONG)
        .show()
        resetGame()
    }

    fun restoreGame() {
        val restoredScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = restotedScore

        val restoredTime = getString(R.string.your_score, Integer.toString(timeLeft))
        timeleftTextView.text = restotedTime

        CountDownTimer = object : CountDownTimer(timeLeft.toLong(), countDownInterval) {

            override fun onTick(millisUntilFinished: Long){

                timeLeft = millisUntilFinished.toInt() / 1000

                timeleftTextView.text = getString(R.string.time_left, timeLeft.toString)
            }
        }
    }

    override fun OnSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState!!.putInt(SCORE_KEY, score)
        outState!!.putInt(TIME_LEFT_KEY, timeLeft)
        countDownTimer.cancel()

        Log.d(TAG, msg:"onSaveInstance State: score =  $score & timeLeft = $timeLeft")
    }

    override fun onDestroy(){
        super.onDestroy()

        Log.d
    }
}


//connect view to activity go
